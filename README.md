# Machine learning basics

This project contains several jupyter notebooks with basic ML operations.
* cluster analysis
* classification
* neural networks
* convolutional neural network
* neural network for regression problem
